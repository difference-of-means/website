import Vue from 'vue'
import Vuex from 'vuex'
import averages from './modules/averages'
import dots from './modules/dots'
import numbers from './modules/numbers'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        averages,
        dots,
        numbers
    },
    strict: debug
})