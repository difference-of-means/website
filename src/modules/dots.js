import Vue from 'vue'

const state = {
    dots: {}
};

const getters = {
    getDots: (state) => {
        return state.dots
    }
};

const mutations = {
    setDot(state, payload) {
        Vue.set(state.dots, payload.index, payload.amount)
    },
    changeDot(state, payload) {
        let totalDots;
        if (state.dots[payload.label]) {
            totalDots = state.dots[payload.label]
        } else {
            totalDots = 0;
        }
        if (payload.operation === '+') {
            totalDots++;
        } else if (payload.operation === '-') {
            totalDots--;
        }
        Vue.set(state.dots, payload.label, totalDots)
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations
}