import Vue from 'vue'

const state = {
    averages: [0, 0]
};

const getters = {
    getAverages: (state) => {
        return state.averages
    }
};

const mutations = {
    setAverage(state, payload) {
        Vue.set(state.averages, payload.index, payload.average)
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations
}