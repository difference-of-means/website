import Vue from 'vue'

const state = {
    numbers: {}
};

const getters = {
    getNumbers: (state) => (id) => {
        return state.numbers[id]
    },
    getTotalById: (state) => (id) => {
        let total = 0;
        state.numbers[id].forEach(e => {
            total = total + parseInt(e)
        });
        return total;
    }
};

const mutations = {
    setNumber(state, payload) {
        Vue.set(state.numbers[payload.id], payload.key, payload.value);
    },
    createNumbers(state, payload) {
        Vue.set(state.numbers, payload.id, payload.values);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations
}